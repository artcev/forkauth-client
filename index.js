const superagent = require('superagent')
const Agent = require('agentkeepalive').HttpsAgent
let keepaliveAgent = new Agent({
  maxSockets: 100,
  maxFreeSockets: 10,
  timeout: 60000, // active socket keepalive for 60 seconds
  freeSocketTimeout: 30000 // free socket keepalive for 30 seconds
})
const request = superagent
const passport = require('passport')
const Strategy = require('passport-custom')
let token
let gUser

class Client {

  constructor (options) {
    this.server = options.server
    this.apiKey = options.apiKey
    this.app = options.app

    if (options.ca) {
      request.ca(options.ca)
    }

    const fAuth = new Strategy(async (req, done) => {
      token = req.headers.token

      let user = await this.token(token)

      if (user.status === 'error') {
        done(null, false)
      } else {
        user = user.data.user
        gUser = user
        done(null, user)
      }
    })

    passport.use('fauth', fAuth)
  }

  setToken (newToken) {
    token = newToken
  }

  initialize () {
    return passport.initialize()
  }

  authenticate (strategy = 'fauth', opts = { session: false }) {
    return passport.authenticate(strategy, opts)
  }

  async login (username = '', password = '') {
    try {
      let response = await request.post(`${this.server}/api/authentication/login`).agent(keepaliveAgent)
        .send({ username: username, password: password, app: this.app })
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response
      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async token (token) {
    try {
      let response = await request.post(`${this.server}/api/authentication/token`).agent(keepaliveAgent)
        .set('token', token).send({ app: this.app })
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response
      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async getRoles () {
    try {
      let response = await request.get(`${this.server}/api/authorizations/roles`).agent(keepaliveAgent)
        .set('token', token)
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response
      }

    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async checkUserRole (role) {
    try {
      let response = await request.get(`${this.server}/api/authorizations/roles`).agent(keepaliveAgent)
        .set('token', token)
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        if (response.status === 'error') {
          return response
        } else {
          let roles = response.data.roles

          for (let i = 0; i < roles.length; i++) {
            if (roles[i].name === role) {
              return { status: 'success', data: { access: true } }
            }
          }

          return { status: 'success', data: { access: false } }
        }
      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async addRole (role) {
    try {
      let response = await request.post(`${this.server}/api/authorizations/roles/add`).agent(keepaliveAgent)
        .set('token', token)
        .send({ name: role })
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response

      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async removeRole (role) {
    try {
      let response = await request.post(`${this.server}/api/authorizations/roles/remove`).agent(keepaliveAgent)
        .set('token', token)
        .send({ name: role })
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response

      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async checkUserAccess () {
    try {
      let response = await request.post(`${this.server}/api/authorizations/apps/check`).agent(keepaliveAgent)
        .set('token', token)
        .send({ app: this.app })
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response
      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async getAllUserPermissions () {
    try {
      let response = await request.get(`${this.server}/api/authorizations/permissions`).agent(keepaliveAgent)
        .set('token', token)
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response
      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async getUserAppPermissions () {
    try {
      let response = await request.get(`${this.server}/api/authorizations/permissions/${this.app}`).agent(keepaliveAgent)
        .set('token', token)
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response
      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async checkUserPermission (action, resource, scope) {
    try {
      let response = await request.post(`${this.server}/api/authorizations/permissions/${this.app}/check`).agent(keepaliveAgent)
        .set('token', token)
        .send({ action: action, resource: resource, scope: scope })
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response
      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async grantPermission (type, type_id, actions, resource, scope, risk, description = '') {
    try {
      let response = await request.post(`${this.server}/api/permissions/create`).agent(keepaliveAgent)
        .set('token', token)
        .send({
          type: type,
          type_id: type_id,
          app: this.app,
          actions: actions,
          resource: resource,
          scope: scope,
          risk: risk,
          description: description
        })
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response
      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async revokePermission (permission_id) {
    try {
      let response = await request.get(`${this.server}/api/permissions/delete/${permission_id}`).agent(keepaliveAgent)
        .set('token', token)
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response
      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async resourcesList () {
    try {
      let response = await request.get(`${this.server}/api/authorizations/resources/${this.app}`).agent(keepaliveAgent)
        .set('token', token)
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response
      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async actionsList (resource) {
    try {
      let response = await request.post(`${this.server}/api/authorizations/actions/${this.app}`).agent(keepaliveAgent)
        .set('token', token)
        .send({ resource: resource })
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response
      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async scopesList (resource, field) {
    try {
      let response = await request.post(`${this.server}/api/authorizations/scopes/${this.app}`).agent(keepaliveAgent)
        .set('token', token)
        .send({ resource: resource, field: field })
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response
      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async appsList () {
    try {
      let response = await request.get(`${this.server}/api/authorizations/apps`).agent(keepaliveAgent)
        .set('token', token)
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response
      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async uploadAvatar (photo, name = undefined) {
    try {
      let response = await request.put(`${this.server}/api/users/avatar/${gUser._id}`).agent(keepaliveAgent)
        .attach('avatar', photo, name)
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response
      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

  async getAvatarBase64 () {
    try {
      let response = await request.get(`${this.server}/api/users/avatar/base64`).agent(keepaliveAgent)
        .set('token', token)
      response = response.body

      if (!response) {
        return { status: 'error', error: { name: 'NoResponse', message: 'No response from the server' } }
      } else {
        return response
      }
    } catch (err) {
      return { status: 'error', error: { name: err.name, message: err.message } }
    }
  }

}

module.exports = Client
