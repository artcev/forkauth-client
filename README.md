**ForkAuth Client**

_Install to Project:_

`npm install forkauth-client --save`

_How To Use:_

Import the ForkAuth Client to your project as follows:
````
const fClient = require('forkauth-client')

const options = {
    server: 'URL for your online ForkAuth server'.
    app: 'The application name in which you are using ForkAuth'
    apiKey: 'The apiKey that is registered for the app in ForkAuth',
    ca: 'Optional. Certificate to sign https requests with.'
}

const fAuth = new fClient(options)

fAuth.initialize() // Initializes Passport

module.exports = fAuth //So you can use it throughout your application.
````
_How to use ForkAuth with Passport:_

The ForkAuth-Client ships with a custom passport strategy that exports as a middleware.

Example Code:
````
const fAuth = require('/path/to/where/you/imported/the/client')

router.get('url', fAuth.authenticate(), (req, res, next) => {
    // Your Logic.
})
````
Example Header:
````
Header Name: 'token'
Header Value: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6Ijg5ZTM5OWYwNjEzMmUyZmNjZmYwZDI1ZDVjNzYxY2U4ODYwODU5IiwiaWF0IjoxNTMxODMxNDg3LCJleHAiOjE1NDA0NzE0ODd9.5YK4xy2zefZqjOGB4ryQdBdW1Q8vReA29Q_A90qcEDk'
````
_Standard response format:_

Error:
````
    status: 'error',
    error: {
        name: 'ErrorName',
        message: 'This is a description of the error'
    }
````

Success:
````
    status: 'success',
    message?: 'Success Message. Optional.',
    data?: {
        [field]: { // The field differs depending on the method called. Only appears when there is data returned.
            // Returned Data        
        }
    }
````
_Available Methods:_

**NOTE - All methods except the login and token methods NEEDS to use the authenticate() passport middleware shipped with this client.**

login(username, password): `fAuth.login('test', 'password') - Login through ForkAuth.`

Response:
````
    status: 'success',
    data: {
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IjMzN2NlYjQ1OTU0YjhhOTFiZmZkODkzNWRlMTE1OTJlNDc1ZjNkIiwiaWF0IjoxNTMxOTg4MjMwLCJleHAiOjE1NDA2MjgyMzB9.gaiEDX2mBBu9kMqRt132-lq8tIN_ea9fuNhgCIRLtGE',
        user: {
            _id: '5ad83f68b4d73906b3603c0b'
            username: 'test',
            name: 'Test',
            surname: 'User',
            locked: false,
            provider: 'local',
            avatar: 'https://forkauth.link.com/media/filename.jpg',
            department: 'HR',
            position: 'clerk',
            createdAt: '2018-04-19T07:04:09.187Z',
            active: '2018-07-19T08:10:51.122Z',
            updatedAt: '2018-07-19T08:17:10.501Z'
        }
    }
````
authenticate(): `fAuth.authenticate()(req, res, next) - Middleware to authenticate the user's token.`

If successful it adds an user object to the req object

Response: 
````
    req.user: {
       _id: '5ad83f68b4d73906b3603c0b'
       username: 'test',
       name: 'Test',
       surname: 'User',
       locked: false,
       provider: 'local',
       department: 'HR',
       position: 'clerk',
       createdAt: '2018-04-19T07:04:09.187Z',
       active: '2018-07-19T08:10:51.122Z',
       updatedAt: '2018-07-19T08:17:10.501Z'
    }
````
getRoles(): `fAuth.getRoles() - Get all the roles that the user has.`

Response:
````
{
    status: 'success',
    data: {
        roles: [
            {
                _id: '5a9fb95e3b319f0708fce3d5',
                name: 'Role_Name',
                description: 'Role Description',
                users: ['5a9fb95e3b319f0708fce419'],
                createdAt: '2018-03-07T10:05:18.455Z',
                updatedAt: '2018-05-29T12:36:06.047Z'
            }
        ]
    }
}
````
checkUserRole(role): `fAuth.checkUserRole('ROLE_NAME') - Check if the user has access to a spesific role.`

Response:
````
    status: 'success',
    data: {
        access: true
    }
}
````
addRole(role): `fAuth.addRole('ROLE_NAME') - Assign the user to the role.`

Response:
````
    status: 'success',
    messge: 'User Added to Role'
}
````
removeRole(role): `fAuth.removeRole('ROLE_NAME') - Remove the user from the role`

Response:
````
    status: 'success',
    messge: 'User Removed from Role'
}
````
checkUserAccess(): `fAuth.checkUserAccess() - Check if the user has access to the app.`

Response:
````
    status: 'success',
    data: {
            access: true
    }
}
````
checkUserPermission(action, resource?, scope?): `fAuth.checkUserPermission('delete', 'users', {field: 'username', value: 'user1'}) - Check if the user has access to the specified permissions.`

Response:
````
    status: 'success',
    data: {
        access: true
    }
}
````
getAllUserPermissions(): `fAuth.getAllUserPermissions() - Get a list of all the user's permissions.`

Response:
````
    status: 'success',
    data: {
            permissions: [
                {
                    _id: '5a9fb95e3b319f0708fce3f4'
                    type: 'role'
                    type_id: '5a9fb95e3b319f0708fce3d4'
                    app: 'AppName'
                    actions: ['read', 'write', 'delete']
                    createdAt: '2018-03-07T10:05:18.513Z'
                    description: 'Permission Description'
                    resource: 'users'
                    risk: 1
                    scope: {
                        type: '*'
                    }
                } 
            ]
            
    }
````
getUserAppPermissions(): `fAuth.getUserAppPermissions() - Get all the user's permissions for the app.`

Response:
````
    status: 'success',
    data: {
            permissions: [
                {
                    _id: '5a9fb95e3b319f0708fce3f4'
                    type: 'role'
                    type_id: '5a9fb95e3b319f0708fce3d4'
                    app: 'AppName'
                    actions: ['read', 'write', 'delete']
                    createdAt: '2018-03-07T10:05:18.513Z'
                    description: 'Permission Description'
                    resource: 'users'
                    risk: 1
                    scope: {
                        type: '*'
                    }
                } 
            ]
            
    }
````
grantPermission(type, type_id, actions, resource, scope, risk, description): `fAuth.grantPermission('user' || 'role', '5a9fb95e3b319f0708fce3d4', ['read', 'write', 'delete'], 'users', {type: '*'}, 75, 'Permission Description') - Create the permission specified.`

Response:
````
    status: 'success',
    message: 'Permission Created',
    data: {
        permission: {
            _id: '5a9fb95e3b319f0708fce3f4'
            type: 'role'
            type_id: '5a9fb95e3b319f0708fce3d4'
            app: 'AppName'
            actions: ['read', 'write', 'delete']
            createdAt: '2018-03-07T10:05:18.513Z'
            description: 'Permission Description'
            resource: 'users'
            risk: 75
            scope: {
                type: '*'
            }
        }
    }
````
revokePermission(permission_id): `fAuth.revokePermission('5a9fb95e3b319f0708fce3f4') - Delete the permission specified.`

Response:
````
    status: 'success',
    message: 'Permission Deleted'
````
resourcesList(): `fAuth.resourcesList() - Get a list of resources the user has access to.`

Response:
````
    status: 'success',
    data: {
            resources: ['users', 'posts', 'tokens']
    }
````
actionsList(resource): `fAuth.actionsList('users') - Get a list of actions that the user has for a specified resource`

Response:
````
    status: 'success',
    data: {
            actions: ['read', 'write', 'delete']
    }
````
scopesList(resource, field?): `fAuth.scopesList('users', 'username') - Get a list of scopes the user has access to for a specified resource. Can further narrow it down by specifying a field name.`

Response:
````
    status: 'success',
    data: {
            scopes: [{
                field: 'username,
                type: 'array',
                value: ['user1', 'user2', 'user43']
            }]
    }
````
appsList(): `fAuth.appsList() - Get a list of apps the user has access to.`

Response:
````
    status: 'success',
    data: {
        apps: ['App1', 'App2', 'App3']
    }
````
uploadAvatar(): `fAuth.uploadAvatar(photoPath) - Upload a photo to use as an avatar for the user.`

`NOTE: The 'photoPath' parameter is the path to the file to be uploaded.`

Response:
````
    status: 'success',
    data: {
        user: {
        _id: '5ad83f68b4d73906b3603c0b'
        username: 'test',
        name: 'Test',
        surname: 'User',
        locked: false,
        provider: 'local',
        avatar: 'https://forkauth.link.com/media/filename.jpg',
        department: 'HR',
        position: 'clerk',
        createdAt: '2018-04-19T07:04:09.187Z',
        active: '2018-07-19T08:10:51.122Z',
        updatedAt: '2018-07-19T08:17:10.501Z'
        }
    }
````
getAvatarBase64(): `fAuth.getAvatarBase64(photo) - Get the Avatar Image as a Base64 Data URI.`

`NOTE: Only use this function if you cannot directly access the avatar URL that is sent with the user.`

Response:
````
    status: 'success',
    data: {
        image: 'Base64 Data URI String'
    }
````
